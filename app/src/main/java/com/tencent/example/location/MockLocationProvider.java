package com.tencent.example.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.SystemClock;

/**
 * Created by shaoyang on 2015/10/29.
 */
public class MockLocationProvider {
    String providerName;
    Context ctx;
    public static LocationManager LocationManager;

    public MockLocationProvider(String name, Context ctx) {
        this.providerName = name;
        this.ctx = ctx;

        LocationManager = (LocationManager) ctx.getSystemService(
                Context.LOCATION_SERVICE);
        LocationManager.addTestProvider(providerName, false, false, false, false, false,
                true, true, 0, 5);
        LocationManager.setTestProviderEnabled(providerName, true);
    }

    public void pushLocation(double lat, double lon) {
//         lm = (LocationManager) ctx.getSystemService(
//                Context.LOCATION_SERVICE);
        final boolean isJellyBeanMr1 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
        Location mockLocation = new Location(providerName);
        mockLocation.setLatitude(lat);
        mockLocation.setLongitude(lon);
        mockLocation.setAltitude(0);
        mockLocation.setTime(System.currentTimeMillis());
        mockLocation.setAccuracy(0);
        if (isJellyBeanMr1) {
            mockLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
        }
//        Bundle bundle = new Bundle();
//        bundle.putDouble("lat", lat);
//        bundle.putDouble("lon", lon);
//		bundle.putFloat("accuracy", 0);
//        LocationManager.sendExtraCommand("gps", "force_xtra_injection", bundle);
//        LocationManager.sendExtraCommand("gps", "force_time_injection", bundle);
        LocationManager.setTestProviderLocation(providerName, mockLocation);
    }

    public void shutdown() {
        LocationManager lm = (LocationManager) ctx.getSystemService(
                Context.LOCATION_SERVICE);
        lm.removeTestProvider(providerName);
    }
}
