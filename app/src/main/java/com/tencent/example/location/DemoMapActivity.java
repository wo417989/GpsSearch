package com.tencent.example.location;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.example.location.fence.Utils;
import com.tencent.example.location.utils.DBHelper;
import com.tencent.example.location.utils.LogWriter;
import com.tencent.map.geolocation.TencentLocation;
import com.tencent.map.geolocation.TencentLocationListener;
import com.tencent.map.geolocation.TencentLocationManager;
import com.tencent.map.geolocation.TencentLocationRequest;
import com.tencent.map.geolocation.TencentPoi;
import com.tencent.tencentmap.mapsdk.map.GeoPoint;
import com.tencent.tencentmap.mapsdk.map.MapActivity;
import com.tencent.tencentmap.mapsdk.map.MapView;
import com.tencent.tencentmap.mapsdk.map.Overlay;
import com.tencent.tencentmap.mapsdk.map.Projection;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 在腾讯地图上显示我的位置.
 *
 * <p>
 * 地图SDK相关内容请参考<a
 * href="http://open.map.qq.com/android_v1/index.html">腾讯地图SDK</a>
 */
public class DemoMapActivity extends MapActivity implements
		TencentLocationListener {

	private TextView mStatus;
	private TextView showStatus;
	private MapView mMapView;
	private LocationOverlay mLocationOverlay;

	private TencentLocation mLocation;
	private TencentLocationManager mLocationManager;

	// 用于记录定位参数, 以显示到 UI
	private String mRequestParams;
	MockLocationProvider mock;
	private Thread thread;// 需要一个线程一直刷新
	private Boolean RUN = true;

	Timer timer = new Timer();
	Timer timerChangeGps = new Timer();

	private double currentGpsLat;
	private double currentGpsLog;
	public double gapGpsDistance = 0.02;

	private LogWriter mLogWriter;
	private static final String TAG = DemoMapActivity.class.getSimpleName();

	private double leftTopLat = 34.190089;
	private double leftTopLog = 115.198965;
	private double rightTopLat = 34.06769;
	private double rightTopLog = 119.172782;
	private double leftBottomLat = 29.753759;
	private double leftBottomLog = 115.217363;
	private double rightBottomLat = 29.817954;
	private double rightBottomLog = 119.172782;
	private String currentCity = "";
	private boolean isCollectSuccess = false;// 是否搜集成功
	private boolean isResearch; //是否是搜集成功的重新搜集

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_demo_map);
//This for test auto build
		// This for test android studio git use
		if (savedInstanceState == null) {
			iniVariable();
		} else {
			this.leftTopLog = Double.valueOf(savedInstanceState.getString("leftTopLog")).doubleValue();
			this.leftTopLat = Double.valueOf(savedInstanceState.getString("leftTopLat")).doubleValue();
			this.rightBottomLog = Double.valueOf(savedInstanceState.getString("rightBottomLog")).doubleValue();
			this.rightBottomLat = Double.valueOf(savedInstanceState.getString("rightBottomLat")).doubleValue();
		}


		File basePathfile = LogWriter.getFileDir(this);
		if (!basePathfile.exists()) {
			basePathfile.mkdirs();
		}

		File logFile = new File(basePathfile.getAbsolutePath()+"/"+currentCity+".txt");
		if (logFile != null && logFile.exists()) {
//			SharedPreferences mySharedPreferences= getSharedPreferences("gps_search",
//					Activity.MODE_PRIVATE);
//			String currentGpsLat = mySharedPreferences.getString("currentGpsLat", "");
//			String currentGpsLog = mySharedPreferences.getString("currentGpsLog", "");
//
//			if (!currentGpsLog.equals("") && !currentGpsLog.equals("")) { //存在就继续存入
//
//			} else {
//				boolean delete = logFile.delete();
//				Log.e(TAG, "delete file is " + delete);
//			}

		}
		if (logFile == null || !logFile.exists()) {
			try {
				if (!logFile.exists()) {
					logFile.createNewFile();
				}
			} catch (Exception e) {

			}
		}
		try {
			mLogWriter = LogWriter.open(logFile ,isResearch);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d(TAG, e.getMessage());
		}

		mStatus = (TextView) findViewById(R.id.status);
		mStatus.setTextColor(Color.RED);

		showStatus = (TextView) findViewById(R.id.showStatus);

		initMapView();

		mLocationManager = TencentLocationManager.getInstance(this);
		// 设置坐标系为 gcj-02, 缺省坐标为 gcj-02, 所以通常不必进行如下调用
		mLocationManager.setCoordinateType(TencentLocationManager.COORDINATE_TYPE_GCJ02);

		mock = new MockLocationProvider(LocationManager.GPS_PROVIDER, this);
		//Set test location
//		mock.pushLocation(31.486382, 120.271225);
		LocationListener lis = new LocationListener() {
			public void onLocationChanged(Location location) {
				//You will get the mock location
			}

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {

			}

			@Override
			public void onProviderEnabled(String provider) {

			}

			@Override
			public void onProviderDisabled(String provider) {

			}
			//...
		};

		MockLocationProvider.LocationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, 1000, 100, lis);
		// 开启线程，一直修改GPS坐标
		/*thread = new Thread(new Runnable() {

			@Override
			public void run() {
				while (RUN) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					EvilTransform evilTransform = new EvilTransform();
					evilTransform.transform(31.486382, 120.271225);
					mock.pushLocation(evilTransform.getMgLat(),evilTransform.getMgLon());
//					mock.pushLocation(31.486382, 120.271225);//31.484442  120.275802
				}
			}
		});
		thread.start();*/

//		currentGpsLat = 22.974487;
//		currentGpsLog = 113.32201;

		currentGpsLat = leftTopLat;
		currentGpsLog = leftTopLog;

		timer.scheduleAtFixedRate(task, 100, 100);

		timerChangeGps.scheduleAtFixedRate(taskChangeGps,500,15000);

		startLocation();
		Toast.makeText(this, "正在搜集数据...请稍后...", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (!isCollectSuccess) {
			SharedPreferences mySharedPreferences= getSharedPreferences("gps_search",
					Activity.MODE_PRIVATE);
			SharedPreferences.Editor editor = mySharedPreferences.edit();
			editor.putString("currentGpsLat", String.valueOf(currentGpsLat));
			editor.putString("currentGpsLog", String.valueOf(currentGpsLog));
			editor.putString(currentCity, "false"); //表示当前这个城市以及数据搜集未成功
			editor.commit();
		}

		outState.putString("leftTopLog", String.valueOf(leftTopLog));
		outState.putString("leftTopLat", String.valueOf(leftTopLat));
		outState.putString("rightBottomLog", String.valueOf(rightBottomLog));
		outState.putString("rightBottomLat", String.valueOf(rightBottomLat));
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState != null) {
			this.leftTopLog = Double.valueOf(savedInstanceState.getString("leftTopLog")).doubleValue();
			this.leftTopLat = Double.valueOf(savedInstanceState.getString("leftTopLat")).doubleValue();
			this.rightBottomLog = Double.valueOf(savedInstanceState.getString("rightBottomLog")).doubleValue();
			this.rightBottomLat = Double.valueOf(savedInstanceState.getString("rightBottomLat")).doubleValue();

//			currentGpsLat = Double.valueOf(savedInstanceState.getString("currentGpsLat")).doubleValue();
//			currentGpsLog = Double.valueOf(savedInstanceState.getString("currentGpsLog")).doubleValue();
		}
	}

	private void iniVariable() {
		if (null != this.getIntent()) {
			String leftTopLog = this.getIntent().getStringExtra("leftTopLog");
			String leftTopLat = this.getIntent().getStringExtra("leftTopLat");
			String rightBottomLog = this.getIntent().getStringExtra("rightBottomLog");
			String rightBottomLat = this.getIntent().getStringExtra("rightBottomLat");
			this.isResearch = this.getIntent().getBooleanExtra("isResearch", false);
			this.currentCity = this.getIntent().getStringExtra("currentCity");
			this.leftTopLog = Double.valueOf(leftTopLog).doubleValue();
			this.leftTopLat = Double.valueOf(leftTopLat).doubleValue();
			this.rightBottomLog = Double.valueOf(rightBottomLog).doubleValue();
			this.rightBottomLat = Double.valueOf(rightBottomLat).doubleValue();
			Log.e(TAG,"currentCity: " +currentCity);
			Log.e(TAG,"leftTopLog: " +leftTopLog);
			Log.e(TAG,"leftTopLat: " +leftTopLat);
			Log.e(TAG,"rightBottomLog: " +rightBottomLog);
			Log.e(TAG,"rightBottomLat: " +rightBottomLat);
		}
	}

	/**
	 * @param msg
	 */
	public void writeLogTofile(String msg) {
		Log.d(TAG, msg);

		try {
			mLogWriter.print(msg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d(TAG, e.getMessage());
		}
	}

	TimerTask task = new TimerTask() {

		@Override
		public void run() {
			mock.pushLocation(currentGpsLat,currentGpsLog);
//			// 需要做的事:发送消息
//			EvilTransform evilTransform = new EvilTransform();
//			evilTransform.transform(currentGpsLat, currentGpsLog);
//			mock.pushLocation(evilTransform.getMgLat(),evilTransform.getMgLon());

		}
	};

	TimerTask taskChangeGps = new TimerTask() {

		@Override
		public void run() {
			Log.e(TAG, "currentGpsLat: "+ currentGpsLat + " rightBottomLat: " +rightBottomLat);
			if (currentGpsLat <= rightBottomLat) {
				Log.e(TAG, "采集数据完成");
				if (DemoMapActivity.this != null && !DemoMapActivity.this.isFinishing()) {
					DemoMapActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(DemoMapActivity.this, "采集数据完成", Toast.LENGTH_SHORT).show();
							if (showStatus != null) {
								showStatus.setText("采集数据完成,文件在SD卡里面的gps目录下");
							}
							isCollectSuccess = true;
							exitSetVara();
							SharedPreferences mySharedPreferences= getSharedPreferences("gps_search",
									Activity.MODE_PRIVATE);
							SharedPreferences.Editor editor = mySharedPreferences.edit();
							editor.putString("currentGpsLat", "");
							editor.putString("currentGpsLog", "");
							editor.putString(currentCity, "true"); //表示当前这个城市以及数据搜集成功
							editor.commit();
						}
					});


				}
//				DemoMapActivity.this.finish();
				return;
			}
			if (currentGpsLog >=rightBottomLog) {
				currentGpsLog = leftTopLog;
				currentGpsLat = currentGpsLat - 0.1;
//				calcLatLog(6,currentGpsLat,currentGpsLog);//向南移
			} else {
				calcLatLog(0,currentGpsLat,currentGpsLog);//向东移
			}

			mock.pushLocation(currentGpsLat,currentGpsLog);

			// 需要做的事:发送消息
//			EvilTransform evilTransform = new EvilTransform();
//			calcLatLog(3,currentGpsLat,currentGpsLog);
//			evilTransform.transform(currentGpsLat, currentGpsLog);
//			mock.pushLocation(evilTransform.getMgLat(),evilTransform.getMgLon());
		}
	};

	private void calcLatLog(int direct ,double lat, double log) {
		switch (direct) {
			case 0:
				currentGpsLog = log +gapGpsDistance;
				break;
			case 1:
				currentGpsLat = lat +gapGpsDistance;
				currentGpsLog = log +gapGpsDistance;
				break;
			case 2:
				currentGpsLat = lat +gapGpsDistance;
				break;
			case 3:
				currentGpsLat = lat +gapGpsDistance;
				currentGpsLog = log -gapGpsDistance;
				break;
			case 4:
				currentGpsLog = log -gapGpsDistance;
				break;
			case 5:
				currentGpsLat = lat -gapGpsDistance;
				currentGpsLog = log -gapGpsDistance;
				break;
			case 6:
				currentGpsLat = lat -gapGpsDistance;
				break;
			case 7:
				currentGpsLat = lat -gapGpsDistance;
				currentGpsLog = log +gapGpsDistance;
				break;

		}
	}

	private void initMapView() {
		mMapView = (MapView) findViewById(R.id.mapviewOverlay);
		mMapView.setBuiltInZoomControls(true);
		mMapView.getController().setZoom(9);

		Bitmap bmpMarker = BitmapFactory.decodeResource(getResources(),
				R.drawable.mark_location);
		mLocationOverlay = new LocationOverlay(bmpMarker);
		mMapView.addOverlay(mLocationOverlay);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
//		stopLocation();
	}

	/**
	 * 退出重置变量
	 */
	private void exitSetVara() {
		RUN = false;
		if (thread != null) {
			thread = null;
		}
		if (task != null) {
			task.cancel();
			task = null;
		}
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		if (taskChangeGps != null) {
			taskChangeGps.cancel();
			taskChangeGps = null;
		}
		if (timerChangeGps != null) {
			timerChangeGps.cancel();
			timerChangeGps = null;
		}
		stopLocation();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		exitSetVara();
		if (!isCollectSuccess) {
			SharedPreferences mySharedPreferences= getSharedPreferences("gps_search",
					Activity.MODE_PRIVATE);
			SharedPreferences.Editor editor = mySharedPreferences.edit();
			editor.putString("currentGpsLat", String.valueOf(currentGpsLat));
			editor.putString("currentGpsLog", String.valueOf(currentGpsLog));
			editor.putString(currentCity, "false"); //表示当前这个城市以及数据搜集未成功
			editor.commit();
		}
	}

	// ===== view listeners
	public void myLocation(View view) {
		if (mLocation != null) {
			mMapView.getController().animateTo(Utils.of(mLocation));
		}
	}

	// ===== view listeners

	// ====== location callback

	@Override
	public void onLocationChanged(TencentLocation location, int error,
			String reason) {
		if (error == TencentLocation.ERROR_OK) {
			mLocation = location;

			// 定位成功
			StringBuilder sb = new StringBuilder();
			sb.append("定位参数=").append(mRequestParams).append("\n");
			sb.append("(纬度=").append(location.getLatitude()).append(",经度=")
					.append(location.getLongitude()).append(",精度=")
					.append(location.getAccuracy()).append("), 来源=")
					.append(location.getProvider()).append(", 地址=")
					.append(location.getAddress());
			StringBuilder sb2 = new StringBuilder();
			String[] strArray = new String[8];
			if (0 != location.getLongitude()) {
				sb2.append(" 经度: " + location.getLongitude());
			}
			if (0 != location.getLatitude()) {
				sb2.append(" 纬度: " + location.getLatitude());
			}
			if ( null != location.getNation()) {
				sb.append("Nation: " + location.getNation());
				sb2.append(" 国家: " + location.getNation());
				strArray[0] = location.getNation().toString();
			} else {
				Log.e(TAG,"nation: null");
				return;
			}
			if ( null != location.getProvince()) {
				sb.append("Province: " + location.getProvince());
				sb2.append(" 省: " + location.getProvince());
				strArray[1] = location.getProvince().toString();
			}
			if ( null != location.getCity()) {
				sb.append("City: " + location.getCity());
				sb2.append(" 市: " + location.getCity());
				strArray[2] = location.getCity().toString();
			}

            if ( null != location.getDistrict()) {
                sb.append("District: " + location.getDistrict());
				sb2.append(" 区县: " + location.getDistrict());
				strArray[3] = location.getDistrict().toString();
			}

            if ( null != location.getStreet()) {
                sb.append("street: " + location.getStreet());
				sb2.append(" 街道(路): " + location.getStreet());
				strArray[4] = location.getStreet().toString();
            }

            if ( null != location.getStreetNo()) {
				sb.append("StreetNo: " + location.getStreetNo());
                sb2.append(" 街道号: " + location.getStreetNo());
				strArray[5] = location.getStreetNo().toString();
            }

            if ( null != location.getAreaStat()) {
                sb.append("AreaStat: " + location.getAreaStat().toString());
            }

            if ( null != location.getTown()) {
                sb.append("Town: " + location.getTown());
				sb2.append(" 乡镇: " + location.getTown());
				strArray[6] = location.getTown().toString();
            }
            if ( null != location.getVillage()) {
                sb.append("Village: " + location.getVillage());
				sb2.append(" 村: " + location.getVillage());
				strArray[7] = location.getVillage().toString();
			}

            if ( null != location.getPoiList() && location.getPoiList().size() > 0) {
                TencentPoi tencentPoi = location.getPoiList().get(0);
                if ( null != tencentPoi){
                    sb.append("poi: " + tencentPoi.getName());
                }
            }

			// 更新 status
			mStatus.setText(sb.toString());


			writeLogTofile(sb2.toString());
			Log.e("DB","strArray: " +strArray[7]);
			DBHelper dbHelper = DBHelper.getInstance(DemoMapActivity.this);
//			DBHelper dbHelper = new DBHelper(DemoMapActivity.this);
			dbHelper.save(strArray);

			// 更新 location 图层
			mLocationOverlay.setAccuracy(mLocation.getAccuracy());
			mLocationOverlay.setGeoCoords(Utils.of(mLocation));
			mMapView.invalidate();
		}
	}

	@Override
	public void onStatusUpdate(String name, int status, String desc) {
		// ignore
	}

	// ====== location callback

	private void startLocation() {
		TencentLocationRequest request = TencentLocationRequest.create();
		request.setInterval(10000);
        request.setRequestLevel(TencentLocationRequest.REQUEST_LEVEL_ADMIN_AREA);
//		mLocationManager
//				.setCoordinateType(TencentLocationManager.COORDINATE_TYPE_WGS84);
        mLocationManager.requestLocationUpdates(request, this);

		mRequestParams = request.toString() + ", 坐标系="
				+ DemoUtils.toString(mLocationManager.getCoordinateType());
	}

	private void stopLocation() {
		if (mLocationManager != null) {
			mLocationManager.removeUpdates(this);
		}
	}

}

class LocationOverlay extends Overlay {

	GeoPoint geoPoint;
	Bitmap bmpMarker;
	float fAccuracy = 0f;

	public LocationOverlay(Bitmap mMarker) {
		bmpMarker = mMarker;
	}

	public void setGeoCoords(GeoPoint point) {
		if (geoPoint == null) {
			geoPoint = new GeoPoint(point.getLatitudeE6(),
					point.getLongitudeE6());
		} else {
			geoPoint.setLatitudeE6(point.getLatitudeE6());
			geoPoint.setLongitudeE6(point.getLongitudeE6());
		}
	}

	public void setAccuracy(float fAccur) {
		fAccuracy = fAccur;
	}

	@Override
	public void draw(Canvas canvas, MapView mapView) {
		if (geoPoint == null) {
			return;
		}
		Projection mapProjection = mapView.getProjection();
		Paint paint = new Paint();
		Point ptMap = mapProjection.toPixels(geoPoint, null);
		paint.setColor(Color.BLUE);
		paint.setAlpha(8);
		paint.setAntiAlias(true);

		float fRadius = mapProjection.metersToEquatorPixels(fAccuracy);
		canvas.drawCircle(ptMap.x, ptMap.y, fRadius, paint);
		paint.setStyle(Style.STROKE);
		paint.setAlpha(200);
		canvas.drawCircle(ptMap.x, ptMap.y, fRadius, paint);

		if (bmpMarker != null) {
			paint.setAlpha(255);
			canvas.drawBitmap(bmpMarker, ptMap.x - bmpMarker.getWidth() / 2,
					ptMap.y - bmpMarker.getHeight() / 2, paint);
		}

		super.draw(canvas, mapView);
	}


}
