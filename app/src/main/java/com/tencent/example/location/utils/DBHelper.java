package com.tencent.example.location.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by shaoyang on 2015/11/3.
 */
public class DBHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "address.db";

    protected static final String ADDRESS_TABLE_NAME	= "gps_address";
    protected static final String ADDRESS_TABLE_ID_KEY		= "id";
    protected static final String ADDRESS_TABLE_NATION_KEY	= "nation";
    protected static final String ADDRESS_TABLE_PROVICE_KEY	= "provice";
    protected static final String ADDRESS_TABLE_CITY_KEY	= "city";
    protected static final String ADDRESS_TABLE_DISTRICT_KEY	= "district";
    protected static final String ADDRESS_TABLE_STREET_KEY	= "street";
    protected static final String ADDRESS_TABLE_STREET_NO_KEY	= "street_no";
    protected static final String ADDRESS_TABLE_TOWN_KEY	= "town";
    protected static final String ADDRESS_TABLE_VILLAGE_KEY	= "village";

    private static DBHelper openHelper;
    private static DatabaseHelper dbHelper;

    private DBHelper(Context context) {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(context);
        }
    }

    public static DBHelper getInstance(Context context) {
        if (openHelper == null) {
            openHelper = new DBHelper(context);
        }
        return openHelper;
    }

    protected static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            if (DATABASE_VERSION == 1) {
                context.deleteDatabase(DATABASE_NAME);
            }
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.w("DB","DatabaseHelper: onCreate");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + ADDRESS_TABLE_NAME + " (" + ADDRESS_TABLE_ID_KEY + " INTEGER primary key autoincrement, "
                    + ADDRESS_TABLE_NATION_KEY + " TEXT, " + ADDRESS_TABLE_PROVICE_KEY + " TEXT, "
                    +ADDRESS_TABLE_CITY_KEY+" TEXT, "+ADDRESS_TABLE_DISTRICT_KEY+" TEXT, "
                    +ADDRESS_TABLE_STREET_KEY+" TEXT, "+ADDRESS_TABLE_STREET_NO_KEY+" TEXT, "
                    +ADDRESS_TABLE_TOWN_KEY+" TEXT, "+ADDRESS_TABLE_VILLAGE_KEY+" TEXT  )");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + ADDRESS_TABLE_NAME);
            onCreate(db);
        }

    }

    /**
     * ����ÿ����ַ
     *
     * @param strArray
     */
    public static  void save(String[] strArray) {// int threadid,
        // int position
        if (openHelper == null) {
            Log.e("DB","openHelper == null");
            return;
        }
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
//            db.beginTransaction();
            db.execSQL("insert into "+ADDRESS_TABLE_NAME+"("+ ADDRESS_TABLE_NATION_KEY + ","+ ADDRESS_TABLE_PROVICE_KEY +","
                    + ADDRESS_TABLE_CITY_KEY + ","+ ADDRESS_TABLE_DISTRICT_KEY + ","+ ADDRESS_TABLE_STREET_KEY + ","
                    + ADDRESS_TABLE_STREET_NO_KEY + ","+ ADDRESS_TABLE_TOWN_KEY + ","+ ADDRESS_TABLE_VILLAGE_KEY
                    + ") values(?,?,?,?,?,?,?,?)", new Object[]{strArray[0], strArray[1], strArray[2],strArray[3],strArray[4],strArray[5],strArray[6],strArray[7]});
//            db.setTransactionSuccessful();
//            db.endTransaction();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                db.close();
            }catch (Exception e1) {
                e1.printStackTrace();
            }
        } finally {

        }
    }
}
