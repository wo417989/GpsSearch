package com.tencent.example.location;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;


public class InputGpsActivity extends Activity {

    EditText etLeftTopLog;
    EditText etLeftTopLat;
    EditText etRightBottomLog;
    EditText etRightBottomLat;
    EditText etInputCity;

    String currentGpsLat;
    String currentGpsLog;
    String currentCityIsSearched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_gps);
        initView();
        String key = DemoUtils.getKey(this);

        // 检查 key 的结构
        if (TextUtils.isEmpty(key)
                || !Pattern.matches("\\w{5}(-\\w{5}){5}", key)) {
            Toast.makeText(this, "运行前请在manifest中设置正确的key", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences mySharedPreferences= getSharedPreferences("gps_search",
                Activity.MODE_PRIVATE);
        currentGpsLat = mySharedPreferences.getString("currentGpsLat", "");
        currentGpsLog = mySharedPreferences.getString("currentGpsLog", "");
        currentCityIsSearched =  mySharedPreferences.getString(etInputCity.getText().toString(), "false");
        if (!currentGpsLog.equals("") && !currentGpsLog.equals("") && currentCityIsSearched.equals("false")) {
            new AlertDialog.Builder(this).setTitle("提示")
                    .setMessage("上次采集gps数据未完成是否从上次断开的gps点继续采集")
                    .setNegativeButton("否",null)
                    .setPositiveButton("是", mPositiveListener).show();
        }
    }

    DialogInterface.OnClickListener mPositiveListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            SharedPreferences mySharedPreferences= getSharedPreferences("gps_search",
                    Activity.MODE_PRIVATE);
            String rightBottomLog = mySharedPreferences.getString("rightBottomLog", "");
            String rightBottomLat = mySharedPreferences.getString("rightBottomLat", "");
            String currentCity = mySharedPreferences.getString("currentCity", "");

            Intent intent = new Intent(InputGpsActivity.this, DemoMapActivity.class);
            intent.putExtra("leftTopLog",currentGpsLog);
            intent.putExtra("leftTopLat",currentGpsLat);
            intent.putExtra("rightBottomLog",rightBottomLog);
            intent.putExtra("rightBottomLat",rightBottomLat);
            intent.putExtra("currentCity",currentCity);
            intent.putExtra("isResearch", false);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.putString("leftTopLog", currentGpsLog);
            editor.putString("leftTopLat", currentGpsLat);
            editor.putString("rightBottomLog", rightBottomLog);
            editor.putString("rightBottomLat", rightBottomLat);
            editor.putString("currentCity", currentCity);
            editor.commit();
            startActivity(intent);
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void initView() {
        etLeftTopLog = (EditText) this.findViewById(R.id.left_top_log);
        etLeftTopLat = (EditText) this.findViewById(R.id.left_top_lat);
        etRightBottomLog = (EditText) this.findViewById(R.id.right_bottom_log);
        etRightBottomLat = (EditText) this.findViewById(R.id.right_bottom_lat);
        etInputCity = (EditText) this.findViewById(R.id.et_input_city);

        Button btnOk = (Button) this.findViewById(R.id.btn_ok);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etInputCity.getText().toString().equals("")) {
                    Toast.makeText(InputGpsActivity.this, "请输入省或市" ,Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etLeftTopLog.getText().toString().equals("")) {
                    Toast.makeText(InputGpsActivity.this, "请输入起点经度" ,Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etLeftTopLat.getText().toString().equals("")) {
                    Toast.makeText(InputGpsActivity.this, "请输入起点纬度" ,Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etRightBottomLog.getText().toString().equals("")) {
                    Toast.makeText(InputGpsActivity.this, "请输入终点经度" ,Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etRightBottomLat.getText().toString().equals("")) {
                    Toast.makeText(InputGpsActivity.this, "请输入终点经度" ,Toast.LENGTH_SHORT).show();
                    return;
                }
                SharedPreferences mySharedPreferences= getSharedPreferences("gps_search",
                        Activity.MODE_PRIVATE);
                currentCityIsSearched =  mySharedPreferences.getString(etInputCity.getText().toString(), "false");
                if (currentCityIsSearched.equals("true")) {
                    new AlertDialog.Builder(InputGpsActivity.this).setTitle("提示")
                            .setMessage("该城市数据以及搜集完成，是否重新搜集数据？\n注意：如果重新搜集会删除原来搜集好的文件")
                            .setNegativeButton("否",null)
                            .setPositiveButton("是", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SharedPreferences mySharedPreferences= getSharedPreferences("gps_search",
                                            Activity.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = mySharedPreferences.edit();
                                    editor.putString("leftTopLog", etLeftTopLog.getText().toString());
                                    editor.putString("leftTopLat", etLeftTopLat.getText().toString());
                                    editor.putString("rightBottomLog", etRightBottomLog.getText().toString());
                                    editor.putString("rightBottomLat", etRightBottomLat.getText().toString());
                                    editor.putString("currentCity", etInputCity.getText().toString());
                                    editor.commit();

                                    Intent intent = new Intent(InputGpsActivity.this, DemoMapActivity.class);
                                    intent.putExtra("leftTopLog", etLeftTopLog.getText().toString());
                                    intent.putExtra("leftTopLat", etLeftTopLat.getText().toString());
                                    intent.putExtra("rightBottomLog", etRightBottomLog.getText().toString());
                                    intent.putExtra("rightBottomLat", etRightBottomLat.getText().toString());
                                    intent.putExtra("currentCity", etInputCity.getText().toString());
                                    intent.putExtra("isResearch", true);
                                    startActivity(intent);
                                }
                            }).show();
                    return;
                }
                SharedPreferences.Editor editor = mySharedPreferences.edit();
                editor.putString("leftTopLog", etLeftTopLog.getText().toString());
                editor.putString("leftTopLat", etLeftTopLat.getText().toString());
                editor.putString("rightBottomLog", etRightBottomLog.getText().toString());
                editor.putString("rightBottomLat", etRightBottomLat.getText().toString());
                editor.putString("currentCity", etInputCity.getText().toString());
                editor.commit();

                Intent intent = new Intent(InputGpsActivity.this, DemoMapActivity.class);
                intent.putExtra("leftTopLog", etLeftTopLog.getText().toString());
                intent.putExtra("leftTopLat", etLeftTopLat.getText().toString());
                intent.putExtra("rightBottomLog", etRightBottomLog.getText().toString());
                intent.putExtra("rightBottomLat", etRightBottomLat.getText().toString());
                intent.putExtra("currentCity", etInputCity.getText().toString());
                intent.putExtra("isResearch", false);
                startActivity(intent);
            }
        });
    }

}
