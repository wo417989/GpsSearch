package com.tencent.example.location.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.apache.http.util.EncodingUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class FileHelper {

    public static String getFilesDirPath(Context context) {
        return context.getFilesDir().getAbsolutePath() + File.separator + "pck";
    }

    /*public static void saveToFile(Context context, Object obj) {
        try {
            File directory = new File(getFilesDirPath(context));
            if (!directory.exists()) {
                directory.mkdirs();
            }
            String absolutePckFileName = getFilesDirPath(context) + File.separator + newRandomUUID() + ".pck";
            PictureItem pictureItem = (PictureItem) obj;
            pictureItem.mAbsolutePckFileName = absolutePckFileName;
            FileOutputStream fos = new FileOutputStream(absolutePckFileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
            oos.flush();
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static PictureItem inflatePictureItemFromFile(String filename) {
        try {
            FileInputStream fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            PictureItem pictureItem = (PictureItem) ois.readObject();
            ois.close();
            return pictureItem;
        } catch (Exception e) {
            deleteFile(new File(filename));
            e.printStackTrace();
            return null;
        }
    }*/

    public static void deleteFile(File file) {
        if (file.exists()) {
            if (file.isFile()) {
                file.delete();
            }
        }
    }

    public static void deleteFile(String absolutFilePath) {
        File file = new File(absolutFilePath);
        if (file.exists()) {
            if (file.isFile()) {
                file.delete();
            }
        }
    }

    private static String newRandomUUID() {
        String uuidRaw = UUID.randomUUID().toString();
        return uuidRaw.replaceAll("-", "");
    }

    public static String getStrFromAssets(Context context, String fileName) {

        try {
            InputStreamReader inputReader = new InputStreamReader(context.getResources().getAssets().open(fileName));
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line = "";
            String Result = "";
            while ((line = bufReader.readLine()) != null) {
                //空行
                if ("".equals(line.trim())) {
                    Result += "\n";
                } else {
                    Result += line;
                }

            }
            return Result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //读SD中的文件
    public static String readFileSdcardFile(Context context,String fileName) throws IOException {
        getFileDir(context);
        String res = "";
        File file = new File(filesDir, fileName);
        if (file == null || !file.exists()) {
            return res;
        }
        String nfilename = filesDir.getAbsolutePath() + "/" + fileName;
        try {
            FileInputStream fin = new FileInputStream(nfilename);
            int length = fin.available();
            byte[] buffer = new byte[length];
            fin.read(buffer);
            res = EncodingUtils.getString(buffer, "UTF-8");
            fin.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;

    }

    public static void deleteFileInfo(Context context,String fileName) {
        getFileDir(context);
        File logFile = new File(filesDir, fileName);
        if (logFile != null && logFile.exists()) {
            boolean delete = logFile.delete();
            Log.e("LOG","delete file is " + delete);
        }
    }

    public static void saveFileInfo(Context context , String text, String fileName) throws Exception {
        getFileDir(context);
        File logFile = new File(filesDir, fileName);
        if (logFile != null && logFile.exists()) {
            boolean delete = logFile.delete();
            Log.e("LOG","delete file is " + delete);
        }
        if (logFile == null || !logFile.exists()) {
            try {
                if (!logFile.exists()) {
                    filesDir.mkdirs();
                    logFile.createNewFile();
                }
            } catch (IOException e) {
                throw new Exception(e);
            }
        }
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(logFile);
            byte[] buf = text.getBytes();
            stream.write(buf);
        } catch (IOException e) {
            throw new Exception(e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException var15) {
                }
            }

        }

    }

    /**
     * 新建文件
     * @param context
     * @param fileName
     * @throws Exception
     */
    public static File createFile(Context context , String fileName) throws Exception {
        getFileDir(context);
        File logFile = new File(filesDir, fileName);
        if (logFile != null && logFile.exists()) {
            boolean delete = logFile.delete();
            Log.e("LOG","delete file is " + delete);
        }
        if (logFile == null || !logFile.exists()) {
            try {
                if (!logFile.exists()) {
                    filesDir.mkdirs();
                    logFile.createNewFile();
                }
            } catch (IOException e) {
                throw new Exception(e);
            }
        }
        return logFile;

    }
    private static Writer mWriter;
    private static SimpleDateFormat df;
    /**
     * 写入文件
     * @param context
     * @param text
     * @param file
     * @throws Exception
     */
    public static void writeInfoToFile(Context context , String text, File file) throws Exception {
        mWriter = new BufferedWriter(new FileWriter(file.getAbsoluteFile()), 2048);
        df = new SimpleDateFormat("[yy-MM-dd hh:mm:ss]: ");
        mWriter.write(df.format(new Date()));
        mWriter.write(text);
        mWriter.write("\n");
        mWriter.flush();

    }

    private static File filesDir;

    public static File getFileDir(Context context) {
        if (filesDir == null) {
            filesDir = new File(Environment.getExternalStorageDirectory(), "gps");
        }
        if (filesDir == null) {
            filesDir = new File(context.getFilesDir(), "gps");
        }
        return filesDir;
    }

}